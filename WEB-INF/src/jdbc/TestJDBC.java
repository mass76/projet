package jdbc;
import java.sql.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class TestJDBC implements ITestJDBC {
  
  Connection conn;
  
  public Connection getConnection() {
    return conn;
  }
  
  // JDBC1 ******************************
  public void connect() throws SQLException, ClassNotFoundException {
    Class.forName("org.sqlite.JDBC");
    conn = DriverManager.getConnection("jdbc:sqlite:data.db");
  }
  
  public boolean checkPasswordBasic( String email, String pwd ) throws SQLException {
    Statement statement = conn.createStatement();
    statement.setQueryTimeout(30);  // set timeout to 30 sec.
    // TODO: Change the request to search for member
    ResultSet rs = statement.executeQuery( "SELECT * FROM member" );
    // TODO: Use ResultSet to check if password match
   while(rs.next()){
	   String mail = rs.getString("email");
     String pass = rs.getString("pwd");
     System.out.println(mail);
     if(mail.equals(email) && pass.equals(pwd)){
	     return true;
     }
   }
    
    return false;
  }
  
  private static String sha1(String password) {
  MessageDigest md = null;
  try {
      md = MessageDigest.getInstance("SHA-1");
  } catch(NoSuchAlgorithmException e) {
      e.printStackTrace();
      return "*****";
  } 
  byte[] sha1sum = md.digest(password.getBytes());
  StringBuffer sb = new StringBuffer();
  for( int i = 0; i < sha1sum.length; i++ ) sb.append( String.format("%02x", sha1sum[i]) );
  return sb.toString();
}
  
  // JDBC2 ******************************
  public boolean exists(String email) throws SQLException {
    // TODO: implement this function
    Statement statement = conn.createStatement();
    statement.setQueryTimeout(30);  // set timeout to 30 sec.
    // TODO: Change the request to search for member
    ResultSet rs = statement.executeQuery( "SELECT * FROM member" );
     while(rs.next()){
	   String mail = rs.getString("email");
    
     if(mail.equals(email)){
	     return true;
     }
     }
    return false;
  }
  
  public void addMember(String lastname, String firstname, String email, String password) throws SQLException {
    // TODO: implement this function
    String encpassword=sha1(password);
    String sql="insert into member values ('"+lastname+"','"+firstname+"','"+email+"','"+encpassword+"')";
     Statement statement = conn.createStatement();
    statement.setQueryTimeout(30);  // set timeout to 30 sec.
    statement.executeUpdate(sql);
  }
  
  public boolean checkPasswordWithSum( String email, String pwd ) throws SQLException {
    // TODO: implement this function
    Statement statement = conn.createStatement();
    statement.setQueryTimeout(30);  // set timeout to 30 sec.
    // TODO: Change the request to search for member
    ResultSet rs = statement.executeQuery( "SELECT * FROM member" );
    // TODO: Use ResultSet to check if password match
   while(rs.next()){
	   String mail = rs.getString("email");
     String pass = rs.getString("pwd");
     System.out.println(mail);
     if(mail.equals(email) && pass.equals(sha1(pwd))){
	     return true;
     }
   }
    
        return false;
  }
  
  public static void main(String argv[]) throws Exception {
    TestJDBC db = new TestJDBC();
    db.connect();
    db.addMember("Diop", "Mamadou", "mass76620@gmail.com", "motdepasse");
    System.out.println( "Test du mot de passe");
    System.out.println(" ==> " + db.checkPasswordWithSum( "mass76620@gmail.com", "motdepasse" ));
  }
  
}