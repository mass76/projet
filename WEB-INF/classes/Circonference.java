

  import java.io.IOException;

  import javax.servlet.ServletException;
  import javax.servlet.annotation.WebServlet;
  import javax.servlet.http.HttpServlet;
  import javax.servlet.http.HttpServletRequest;
  import javax.servlet.http.HttpServletResponse;

  @WebServlet("/circ")
  public class Circonference extends HttpServlet {
  	private static final long serialVersionUID = 1L;

    @Override
  	protected void service( HttpServletRequest req, HttpServletResponse resp ) 
  		throws ServletException, IOException {
  		resp.setCharacterEncoding("UTF-8");

  		// TODO: get parameter "rayon" from request ***************
  		String rayon = null;
  		
  		if ( rayon == null || rayon.isEmpty() ) {
  			resp.setContentType("text/plain");
  			resp.getWriter().write( 
  				req.getRequestURI() + " Utilisation:\n" +
  				"\thttp://" + req.getServerName() + ":" + req.getServerPort() + req.getRequestURI() + "?rayon=X\n" +
  				"\tretourne la circonférence d'un cercle dont le rayon est donné.\n" +
  				"\trayon: un nombre flottant\n"
  			);
  			return;
  		}

  		double d;
  		try {
  			d = Double.parseDouble(rayon);
  		} catch( NumberFormatException exp ) {
  			resp.setContentType("text/plain");
  			resp.getWriter().write( "Le rayon indiqué ["+rayon+"] ne peut pas être lu.\n" );
  			return;
  		}

  		resp.setContentType("application/json");
  		
  		// TODO: compute circumference ***************
  		double circ = 0.0;
  		
  		resp.getWriter().write( "circ=" + circ );
  	}
  }